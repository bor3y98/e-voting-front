import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateVoteComponent } from './create-vote/create-vote.component';
import { AboutComponent } from './about/about.component';
import { CastVoteComponent } from './cast-vote/cast-vote.component';
import { MyPollsComponent } from './my-polls/my-polls.component';
import { VoteResultComponent } from './vote-result/vote-result.component';
import { AuthGuard } from './auth-guard';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyVotesComponent } from './my-votes/my-votes.component';
import { CastPrivateVoteComponent } from './private-vote/cast-private-vote/cast-private-vote.component';
import { PrivateVoteResultComponent } from './private-vote/private-vote-result/private-vote-result.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'my-polls', canActivate: [AuthGuard], component: MyPollsComponent },
  { path: 'my-votes', canActivate: [AuthGuard], component: MyVotesComponent },
  { path: 'password-reset/:token', canActivate: [AuthGuard], component: PasswordResetComponent },
  { path: 'my-profile', canActivate: [AuthGuard], component: EditProfileComponent },


  {
    path: 'vote', children: [
      {
        path: 'create', canActivate: [AuthGuard], component: CreateVoteComponent
      },
      {
        path: 'cast/:id', canActivate: [AuthGuard], component: CastVoteComponent
      }, {
        path: 'result/:id', component: VoteResultComponent
      },

    ]
  },
  

  {
    path: 'pvote', canActivate: [AuthGuard], children: [
      {
        path: 'cast/:id', component: CastPrivateVoteComponent
      }, {
        path: 'result/:id', component: PrivateVoteResultComponent
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
