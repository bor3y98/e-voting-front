import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../global.config';
declare var UIkit: any;
@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  constructor(private route: ActivatedRoute, public http: HttpClient, private router: Router,public global:Globals) {
    this.route.params.subscribe(params => {
      this.http.post('http://localhost:8000/user/reset-password-check-expire', { token: params['token'] }).subscribe(res => {
        if (!res['isError']) {
          this.router.navigate(['']);
          this.global.resetToken=params['token']
          UIkit.modal('#forget2-modal').show();
        }
        else {
          if (!res['expired']) {
            UIkit.modal('#wrong-token-modal').show();
          }
          else {
            UIkit.modal('#expired-token-modal').show();
          }
        }
      })

    })

  }

  ngOnInit(): void {
  }

}
