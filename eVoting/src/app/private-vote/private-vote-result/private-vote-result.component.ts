import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Chart } from 'node_modules/chart.js'
import { CookieService } from 'ngx-cookie-service';
declare var $: any;
declare var UIkit: any;
@Component({
  selector: 'app-private-vote-result',
  templateUrl: './private-vote-result.component.html',
  styleUrls: ['./private-vote-result.component.scss']
})
export class PrivateVoteResultComponent implements OnInit {


  voteDetails;
  voteOptions;
  chartDataset = [];
  voteId;
  isOwner = false;
  maxRange;
  stepSize;
  // colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
  //   '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
  //   '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
  //   '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
  //   '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
  //   '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
  //   '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
  //   '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
  //   '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
  //   '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
  colorArray=['#7f5fcb','#313153','#99ff33'];

  constructor(public route: ActivatedRoute, private http: HttpClient, private cookie: CookieService, private _location: Location) {
    this.route.params.subscribe(params => {
      this.voteId = params['id']
      this.http.post('http://localhost:8000/vote/result', { id: params['id'], token: this.cookie.get('token') }).subscribe(res => {
        this.voteDetails = res['vote'];
        this.voteOptions = res['voteOptions'];
        this.isOwner = res['voteOwner'];
        let max = 0;
        for (let i = 0; i < this.voteDetails['options'].length; i++) {
          let object = {};
          if (this.voteOptions[this.voteDetails['options'][i]] > max) {
            max = this.voteOptions[this.voteDetails['options'][i]];
          }
          object['label'] = this.voteDetails['options'][i];
          object['data'] = [this.voteOptions[this.voteDetails['options'][i]]];
          object['backgroundColor'] = [this.colorArray[i]];
          object['borderColor'] = [this.colorArray[i]];
          this.chartDataset.push(object)
        }
        this.maxRange = 10 * max.toString().length;
        this.stepSize = this.maxRange / 5;
      }, errorRes => {
        if (!errorRes['error']['isVoted']) {
          UIkit.modal('#not-voted-modal').show();
        }
      })
    })
  }

  ngOnInit(): void {
    //attaching chart with canvas and info 
  //   const canvas = <HTMLCanvasElement>document.getElementById("chartCanvas");
  //   const ctx = canvas.getContext("2d");
  //   setTimeout(() => {
  //     var data = {
  //       labels: [''],
  //       datasets: this.chartDataset

  //     };
  //     var options = {
  //       title: {
  //         display: true,
  //         position: "top",
  //         fontSize: 18,
  //         fontColor: "#111"
  //       },

  //       legend: {
  //         display: true,
  //         position: "bottom",
  //         labels: {
  //           fontColor: "red",
  //         }
  //       },
  //       scales: {
  //         yAxes: [{
  //           ticks: {
  //             min: 0,
  //             max: this.maxRange,
  //             stepSize: this.stepSize
  //           }
  //         }],
  //         xAxes: [{
  //           categoryPercentage: 0.7,
  //           barPercentage: 0.5
  //         }],
  //       }
  //     };
  //     var chart = new Chart(ctx, {
  //       type: "bar",
  //       data: data,
  //       options: options,
  //     });
  //   }, 500)
  // }

  const canvas = <HTMLCanvasElement>document.getElementById("chartCanvas");
    const ctx = canvas.getContext("2d");
    setTimeout(() => {
      var data = {
        labels: [''],
        datasets: this.chartDataset

      };
      var options = {
        // title: {
        //   display: false,
        //   position: "bottom",
        //   fontSize: 18,
        //   fontColor: "#111"
        // },

        // element:{
        //   radius:3,
        //   rotation:0,
        //   backgroundColor:"rgba(0, 0, 0, 0.1)"
        // },

        legend: {
          display: true,
          position: "bottom",
          align:"center",
          labels: {
            fontColor: "red",
            padding:60
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 0,
              // max: this.maxRange,
              max:1,
              // stepSize: this.stepSize
              stepSize:0.2
            }
          }],
          xAxes: [{
            categoryPercentage: 0.6,
            // barPercentage: 0.5
            barPercentage: 0.6

          }],
        }
      };
      var chart = new Chart(ctx, {
        type: "bar",
        data: data,
        options: options,
      });
    }, 500)
  }

  //when the user click 
  openPopUP(): void {
    // Get the popUp
    const popUp = document.getElementById("myModal");
    popUp.style.display = "block";
  }
  closePopUP(): void {
    // Get the popUp
    const popUp = document.getElementById("myModal");
    // When the user clicks Image x
    popUp.style.display = "none";
  }
  // When the user clicks anywhere outside of the popUp close it 
  closeOutsidePopUp(): void {
    // Get the popUp
    const popUp = document.getElementById("myModal");
    if (event.target == popUp) {
      popUp.style.display = "none";
    }
  }

  backClicked() {
    this._location.back();
  }
  stopPollAvalible() {
    return (this.isOwner && this.getExpireDate(this.voteDetails['endDate']) !== 'This vote has ended')
  }
  copyURL() {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (window.location.href));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }

  getExpireDate(endDate) {
    var nowDate = new Date();
    var date = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate();
    let currentDate;
    currentDate = date.split('-')
    endDate = endDate.split('-');
    const months = endDate[1] - currentDate[1];
    const days = endDate[0] - currentDate[2];
    const years = endDate[2] - currentDate[0];
    let returnMessage = 'This vote has ended';
    if (years > 0) {
      if (years === 1) {
        returnMessage = 'Ends in ' + years + ' year ';
      } else {
        returnMessage = 'Ends in ' + years + ' years ';
      }
    } else if (months > 0 && years === 0) {
      if (months === 1) {
        returnMessage = 'Ends in ' + months + ' month ';
      } else {
        returnMessage = 'Ends in ' + months + ' months ';
      }
    } else if (days > 0 && months === 0) {
      if (days === 1) {
        returnMessage = 'Ends in ' + days + ' day ';
      } else {
        returnMessage = 'Ends in ' + days + ' days ';
      }
    }
    else if (days === 0 && months === 0) {
      returnMessage = 'Ends today';
    }
    return returnMessage;
  }
  stopPoll() {
    this.http.post('http://localhost:8000/vote/stop', { id: this.voteId, token: this.cookie.get('token') }).subscribe(res => {
      this.voteDetails = res['savedVote']
      this.closePopUP();
    }, errorRes => {
    });
  }

}
