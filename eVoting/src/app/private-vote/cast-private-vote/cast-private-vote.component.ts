import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

declare var UIkit: any;
declare var $: any;

@Component({
  selector: 'app-cast-private-vote',
  templateUrl: './cast-private-vote.component.html',
  styleUrls: ['./cast-private-vote.component.scss']
})
export class CastPrivateVoteComponent implements OnInit {

  voteDetails;
  voteForm: FormGroup;
  voteId;
  constructor(private router: Router, private route: ActivatedRoute, public http: HttpClient, private formBuilder: FormBuilder, public cookie: CookieService) {
    this.voteForm = this.formBuilder.group({
      option: ['', Validators.required],
      id: ['', Validators.required],
    });
    this.route.params.subscribe(params => {
      this.voteId=params['id']
      this.voteForm.controls['id'].setValue(params['id']);
      this.http.post('http://localhost:8000/vote/vote-details/', { id: params['id'], token: this.cookie.get('token') }).subscribe(res => {
        if (!res['isError']) {
          if (res['isVoted']) {
            $('.word').html('You have voted for this poll');
            UIkit.modal('#cast-vote-modal').show();
          }
          this.voteDetails = res['vote'];
        }
        else {
        }
      })
    });

  }

  castVote() {
    if (this.voteDetails.isMulti) {
      let options = []
      this.voteDetails['options'].forEach(vote => {
        if ($('#' + vote).prop('checked')) {
          options.push($('#' + vote).val())
        }
      });
      this.voteForm.get('option').setValue(options)
    }
    if (this.voteForm.invalid) {
      return;
    }
    this.http.post('http://localhost:8000/vote/cast', { data: this.voteForm.value, token: this.cookie.get('token') }).subscribe(res => {
      if (!res['isError']) {
        this.router.navigate(['/pvote/result/'+this.voteForm.controls['id'].value]);
      }
    })
  }
  ngOnInit(): void {
  }

}
