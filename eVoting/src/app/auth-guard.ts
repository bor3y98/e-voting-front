import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
declare var UIkit: any;

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate() {
        if (!this.cookie.get('token') || this.cookie.get('token').length === 0) {
            this.router.navigate(['']);
            UIkit.modal('#log-modal').show();
        }
        return true;
    }

    constructor(private router: Router, private cookie: CookieService) {
    }

}
