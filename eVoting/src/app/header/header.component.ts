import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Globals } from '../global.config';
declare var $: any; // to use jQeury
declare var UIkit: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  signInForm: FormGroup;
  signUpForm: FormGroup;
  resetPasswordForm: FormGroup;
  loggedUserToken;
  resetToken = '';
  constructor(public formBuilder: FormBuilder, public http: HttpClient, private router: Router, public global: Globals,
    public cookies: CookieService) {
    this.loggedUserToken = this.cookies.get('token');
    // signInForm form group
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
    // signUpForm form group
    this.signUpForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      // passwordConfirm: ['', Validators.required],
    });

    this.resetPasswordForm = this.formBuilder.group({
      token: [this.resetToken, Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    });



  }

  switch(): void {
    const element = document.getElementById("largeTwoMobile");
    element.classList.toggle("act");

    const q = document.getElementById("responsiveButton");
    q.classList.toggle("act");

    const w = document.getElementById("dropdown");
    // w.style.bottom="222px";
    w.classList.toggle("profileIconRes");


  }
   asd;
  dropDownOpen():void{
    this.asd=document.getElementById("showORnot");
    this.asd.classList.toggle("dropdownshow");
    
  }
  

  ngOnInit(): void {

    //window.addEventListener('scroll', this.myFunction, true);
    // if (event.target != this.asd) {
    //   // this.asd.style.display = "none";
    // this.asd.classList.toggle("dropdownshow");
      
    // }

  }
  // myFunction() {
  //   const navbar = document.getElementById("navbar");
  //   const sticky = navbar.offsetTop;
  //   if (window.pageYOffset > sticky) {
  //     navbar.classList.add("sticky");
  //   } else {
  //     navbar.classList.remove("sticky");
  //   }
  // }

  isLoggedIn() {
    return (this.cookies.get('token').length === 0);
  }
  onTextChange(event) {
    if ($(event.target).val().length === 0) {
      $(event.target).next().show();
    }
    else {
      $(event.target).next().hide();
      if ($(event.target).hasClass('email-sign-up')) {
        this.validateEmail($(event.target).val(), $(event.target))
      }
    }
  }


  login() {
    const data = this.signInForm.value;
    if (data['email'].length === 0) {
      $("#email-feedback").show();
    }
    if (data['password'].length === 0) {
      $("#password-feedback").show();
    }

    if (this.signInForm.invalid) {  // if login is falid send data to backend ...
      return;
    }
    this.http.post('http://localhost:8000/user/sign-in', data).subscribe(res => {
      if (!res['isError']) {
        this.cookies.set('token', res['token']);
        this.cookies.set('fullName', res['fullName']);
        UIkit.modal('#log-modal').hide();
        this.signInForm.reset();
        this.loggedUserToken = this.cookies.get('token');

      }
      else {
        $("#login-feedback").html(res['message']); // send res.message to html
        $("#login-feedback").show();
      }
    });
  }


  logout() {
    this.cookies.delete('token');
  }




  // signUp ---------------------------------

  signUp() {
    const data = this.signUpForm.value;
    for (let i = 0; i < $('.signup-input').length; i++) {
      if ($($('.signup-input')[i]).val().length === 0) {
        $($('.signup-input')[i]).next().show();
      }
    }
    if (!this.validateEmail($('.email-sign-up').val(), $('.email-sign-up'))) {
      return;
    }
    if (this.signUpForm.invalid) {  // if signUp falied don't send data to backend ...
      return;
    }
    this.http.post('http://localhost:8000/user/create', data).subscribe(res => {
      if (!res['isError']) {
        UIkit.modal('#sign-up-modal').hide();
        UIkit.modal('#log-modal').show();
        this.signUpForm.reset();
      }
      else {
        if (res['message'] !== 'Maximum number of users are 10') {
          $('#sign-up-feedback').html(res['message']);
          $('#sign-up-feedback').show();
        }
      }
    });
  }




  // reset password ....................................
  resetSendEmail() {
    const emailElement = $('#email');
    const email = emailElement.val().replace(/\s/g, '');
    if (email.length > 0) {
      emailElement.next().hide();
      if (this.validateEmail(email, emailElement)) {
        this.http.post('http://localhost:8000/user/reset-password-email', { email }).subscribe(res => {
          UIkit.modal('#forget3-modal').show();
          $('.emailto').html('If this email exists we will send email to ' + email + ' with a link to verify your identity. Click the link in the email to continue.')
        })
      }
    }
    else {
      emailElement.next().html('Enter an email first');
      emailElement.next().show();
    }


  }
  validateEmail(email, emailElement) {
    if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
      emailElement.next().html('Enter a valid email');
      emailElement.next().show();
      return false;
    }
    else {
      emailElement.next().hide();
      return true;

    }
  }
  onPasswordChange(event) {
    const passwordElement = $(event.target);
    const password = passwordElement.val();
    if (password.length === 0) {
      passwordElement.show();
    }
    else {
      let passwordConfirm;
      if (passwordElement.hasClass('passinput')) {
        passwordConfirm = $('#reset-password-confirm').val();
      }
      else {
        // password = $('#reset-password').val()
      }
      if (passwordConfirm !== password && passwordConfirm.length > 0) {
        $('#reset-password-confirm').next().html("Passowrd doesn't match");
        $('#reset-password-confirm').next().show();
      }
      else {
        $('#reset-password-confirm').next().hide();
        passwordElement.next().hide();
      }
    }
  }
  onPasswordConfirmChange(event) {
    const passowrdConfirmElement = $(event.target);
    const passowrdConfirm = passowrdConfirmElement.val();
    if (passowrdConfirm.length === 0) {
      passowrdConfirmElement.next().html('This feild is required');
      passowrdConfirmElement.next().show();
    }
    else {
      let password;
      if (passowrdConfirmElement.hasClass('passinput')) {
        password = $('#reset-password').val()
      }
      else {
        // password = $('#reset-password').val()
      }
      if (passowrdConfirm !== password && password.length > 0) {
        passowrdConfirmElement.next().html("Passowrd doesn't match");
        passowrdConfirmElement.next().show();
      }
      else {
        passowrdConfirmElement.next().hide();
      }

    }
  }
  onEmailChange(event) {
    if ($(event.target).val().length === 0) {
      $(event.target).next().html('This feild is required');
      $(event.target).next().show();
    }
    else {
      if (!this.validateEmail($(event.target).val(), $(event.target()))) {
        $(event.target).next().hide();

      }
    }
  }
  resetPasswordSubmit() {
    this.resetPasswordForm.controls['token'].setValue(this.global.resetToken)
    for (let i = 0; i < $('.passinput').length; i++) {
      if ($($('.passinput')[i]).val().length === 0) {
        $($('.passinput')[i]).next().html('This feild is required');
        $($('.passinput')[i]).next().show();
      }
    }
    if (this.resetPasswordForm.invalid) {
      return;
    }
    this.http.post('http://localhost:8000/user/reset-password', { data: this.resetPasswordForm.value }).subscribe(res => {
      if (!res['isError']) {
        UIkit.modal('#forget2-modal').hide();
        UIkit.modal('#log-modal').show();
      }
    })
  }

}



