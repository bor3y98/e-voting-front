import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
declare var $: any;
declare var UIkit: any;


@Component({
  selector: 'app-create-vote',
  templateUrl: './create-vote.component.html',
  styleUrls: ['./create-vote.component.scss']
})
export class CreateVoteComponent implements OnInit {
  createVoteForm;
  constructor(public formBuilder: FormBuilder, public http: HttpClient, private router: Router, public cookie: CookieService) {
    if (this.router.url === '/vote/create') {
      $(".header-li").removeClass('active');
    }

    this.createVoteForm = this.formBuilder.group({
      title: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      isPublic: [true, Validators.required],
      isMulti: [false, Validators.required],
      options: this.formBuilder.array([]),
      optionsImages: this.formBuilder.array([]),
    });
    this.addOption();
    this.addOption();
    // this.fixPosition();
  }

  ngOnInit() {
    $(document).ready(() => {
      $(window).keydown((event) => {
        if (event.keyCode === 13) {
          event.preventDefault();
          return false;
        }
      });
    });
    // Datepicker formate
    $('#startDate').datepicker({
      dateFormat: 'dd-mm-yy',
      minDate: -0,
      onSelect(startDateText) {
        $(this).next().hide();
        $('#startDateFeedback').fadeOut(500);
        $('#endDate').datepicker('destroy');
        const startDate = $('#startDate').val().split('-');
        $('#endDate').datepicker({
          dateFormat: 'dd-mm-yy',
          minDate: new Date((startDate[2]), startDate[1] - 1, startDate[0]),
          onSelect(endDateText) {
            $(this).next().hide();
          }
        });
      }
    });
  }

  onTextChange(event) {
    if ($(event.target).val().length === 0) {
      $(event.target).next().show();
    }
    else {
      $(event.target).next().hide();
    }
  }


  // Public or private vote
  changePublicStatus(event) {
    if (!$(event.target).hasClass('active')) {
      $('.form-check').removeClass('active');
      $(event.target).parent().addClass('active');
      this.createVoteForm.controls['isPublic'].setValue($(event.target).val() === 'true');
    }
  }


  // Options operations
  addOption() {
    // this.fixPosition();
    const options = this.createVoteForm.get('options') as FormArray;
    options.push(this.formBuilder.control(''));
    const optionsImages = this.createVoteForm.get('optionsImages') as FormArray;
    optionsImages.push(this.formBuilder.control(''));
  }
  deleteOption(index) {
    if (index) {
      const options = this.createVoteForm.get('options') as FormArray;
      const optionsImages = this.createVoteForm.get('optionsImages') as FormArray;
      options.removeAt(index);
      optionsImages.removeAt(index);

    }
  }
  deleteImage(index) {
    const optionsImages = this.createVoteForm.get('optionsImages') as FormArray;
    optionsImages.at(index).setValue('')

  }
  addOptionText(index, event) {
    const options = this.createVoteForm.get('options') as FormArray;
    options.controls[index].setValue($(event.target).val());
  }


  // On form submit
  createVote() {
    this.createVoteForm.controls['startDate'].setValue($('#startDate').val());
    this.createVoteForm.controls['endDate'].setValue($('#endDate').val());
    let isError = false;
    for (let i = 0; i < $('.form-control').length; i++) {
      if ($($('.form-control')[i]).val().length === 0) {
        $($('.form-control')[i]).next().show();
      }
    }
    this.createVoteForm.value['options'].forEach(element => {
      if (this.createVoteForm.value['options'].filter(i => i === element).length > 1) {
        $('#form-invalid-feedback').html('Option names should be unique')
        $('#form-invalid-feedback').show()
        $('#form-invalid-feedback').delay(3000).fadeOut(200)
        isError = true;
      }
    });
    if (this.createVoteForm.invalid || isError) {
      return;
    }
    const data = this.createVoteForm.value;
    this.http.post('http://localhost:8000/vote/create', { data, 'token': this.cookie.get('token') }).subscribe(res => {
      if (!res['isError']) {
        if (res['privateLink'].length > 0) {
          $('.private-link').show();
          $('#private-text').html(res['privateLink']);
        }
        UIkit.modal($('#confirm-modal')).show();
        this.createVoteForm.reset()
      }
    });
  }

  fileUploads(event: any, index: any) {
    const file = event.target.files;
    const optionsImages = this.createVoteForm.get('optionsImages') as FormArray;
    const reader = new FileReader();
    reader.onload = (e) => {
      const base64 = reader.result + '';
      optionsImages.controls[index].setValue(this.formBuilder.control(base64));
    };
    reader.readAsDataURL(file[0]);
  }
  copyToClipboard() {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', ($('#private-text').text()));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }

  fixPosition() {

    //const f = document.getElementById("fixImage");

    //f.style.cursor = "none";
    //       const c = document.getElementById("fixImage");

    //   //  console.log(c);


    //    if(c!=null){
    //    c.style.top="-55px";
    //   }

    // let elms = document.querySelectorAll("[id='duplicateID']");
    // let i = 0;
    // for( i;i < elms.length; i++) 
    // if(document.querySelectorAll('#fixImage')!=null)
    // (document.querySelectorAll('#fixImage')).style.top = '-55px';
    // this.divStyle = 200; 
  }


}