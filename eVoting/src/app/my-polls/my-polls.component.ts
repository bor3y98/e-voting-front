import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
declare var $: any;
@Component({
  selector: 'app-my-polls',
  templateUrl: './my-polls.component.html',
  styleUrls: ['./my-polls.component.scss']
})
export class MyPollsComponent implements OnInit {
  votesList;
  myName;
  constructor(public router: Router, private http: HttpClient, public cookie: CookieService) {
    if (this.router.url === '/my-polls') {
      $(".header-li").removeClass('active');
      $('.header-li.my-polls').addClass('active');
    }
    this.http.post('http://localhost:8000/vote/my-votes/', { token: this.cookie.get('token') }).subscribe(res => {
      this.votesList = res['votes'];
      this.myName = this.cookie.get('fullName');
    });
  }
  ngOnInit(): void {
  }

  getExpireDate(endDate) {
    var nowDate = new Date();
    var date = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate();
    let currentDate;
    currentDate = date.split('-')
    endDate = endDate.split('-');
    const months = endDate[1] - currentDate[1];
    const days = endDate[0] - currentDate[2];
    const years = endDate[2] - currentDate[0];
    let returnMessage = 'Expired';
    if (years > 0) {
      if (years === 1) {
        returnMessage = 'Ends in ' + years + ' year ';
      } else {
        returnMessage = 'Ends in ' + years + ' years ';
      }
    } else if (months > 0 && years === 0) {
      if (months === 1) {
        returnMessage = 'Ends in ' + months + ' month ';
      } else {
        returnMessage = 'Ends in ' + months + ' months ';
      }
    } else if (days > 0 && months === 0) {
      if (days === 1) {
        returnMessage = 'Ends in ' + days + ' day ';
      } else {
        returnMessage = 'Ends in ' + days + ' days ';
      }
    }
    else if (days === 0 && months === 0) {
      returnMessage = 'Ends today';
    }
    return returnMessage;
  }
}
