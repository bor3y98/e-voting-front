import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  voteList;
  usersList;
  constructor(private router: Router, public http: HttpClient) {
    if (this.router.url === '/') {
      $(".header-li").removeClass('active');
      $('.header-li.explore').addClass('active')
    }
    this.http.get('http://localhost:8000/vote/').subscribe(res => {
      this.voteList = res['votesArr'];
      this.usersList = res['voteUsers'];
    })
  }

  ngOnInit(): void {
    
    const input = document.getElementById('searchbar') as HTMLInputElement;
    const filter = document.getElementById('filter') as HTMLInputElement;
    input.addEventListener('keyup', (event) => {
      if (input.value === '') {
        filter.setAttribute('uk-filter-control', '');
      } else {
        filter.setAttribute('uk-filter-control', '[data-name*=\'' + input.value.toUpperCase() + '\']');
      }
      filter.click();
    });
  }

  getExpireDate(endDate) {
    var nowDate = new Date();
    var date = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate();
    let currentDate;
    currentDate = date.split('-')
    endDate = endDate.split('-');
    const months = endDate[1] - currentDate[1];
    const days = endDate[0] - currentDate[2];
    const years = endDate[2] - currentDate[0];
    let returnMessage = '';
    if (years > 0) {
      if (years === 1) {
        returnMessage = 'Ends in ' + years + ' year ';
      } else {
        returnMessage = 'Ends in ' + years + ' years ';
      }
    } else if (months > 0 && years === 0) {
      if (months === 1) {
        returnMessage = 'Ends in ' + months + ' month ';
      } else {
        returnMessage = 'Ends in ' + months + ' months ';
      }
    } else if (days > 0 && months === 0) {
      if (days === 1) {
        returnMessage = 'Ends in ' + days + ' day ';
      } else {
        returnMessage = 'Ends in ' + days + ' days ';
      }
    }
    else if (days === 0 && months === 0) {
      returnMessage = 'Ends today';
    }
    return returnMessage;
  }
}
