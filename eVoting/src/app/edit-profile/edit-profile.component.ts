import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { isError } from 'util';
declare var $: any;
declare var UIkit: any;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  editProfileForm;
  changePasswordForm;
  constructor(public formBuilder: FormBuilder, public http: HttpClient, private router: Router,
    public cookie: CookieService) {
    this.editProfileForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
    });

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      newPasswordConfirm: ['', Validators.required],
      token: ['']
    });
    this.http.post('http://localhost:8000/user/get-user-data', { 'token': this.cookie.get('token') }).subscribe(res => {
      this.editProfileForm.controls['name'].setValue(res['name']);
      this.editProfileForm.controls['email'].setValue(res['email']);
    });
  }
  ngOnInit(): void {
  }
  //On form submit
  saveChanges() {
    const data = this.editProfileForm.value;
    this.http.post('http://localhost:8000/user/edit', { data, 'token': this.cookie.get('token') }).subscribe(res => {
      this.cookie.set("token", res['token']);
      this.cookie.set("fullName", res['fullName']);
      $('#edit-profile-valid').show();
      $('#edit-profile-valid').delay(5000).fadeOut(500);
    })
  }

  onTextChange(event) {
    if ($(event.target).val().length === 0) {
      $(event.target).next().show();
    }
    else {
      $(event.target).next().hide();
    }
  }
  onPasswordChange(event) {
    const passwordElement = $(event.target);
    const password = passwordElement.val();
    if (password.length === 0) {
      passwordElement.next().html("This feild is required");
      passwordElement.next().show();
    }
    else {
      let passwordConfirm;
      if (passwordElement.hasClass('passinput')) {
        passwordConfirm = $('#confirm-reset-password').val();
      }
      else {
        // password = $('#reset-password').val()
      }
      if (passwordConfirm !== password && passwordConfirm.length > 0) {
        $('#confirm-reset-password').next().html("Passowrd doesn't match");
        $('#confirm-reset-password').next().show();
      }
      else {
        $('#confirm-reset-password').next().hide();
        passwordElement.next().hide();
      }
    }
  }

  onPasswordConfirmChange(event) {
    const passowrdConfirmElement = $(event.target);
    const passowrdConfirm = passowrdConfirmElement.val();
    if (passowrdConfirm.length === 0) {
      passowrdConfirmElement.next().html('This feild is required');
      passowrdConfirmElement.next().show();
    }
    else {
      let password;
      if (passowrdConfirmElement.hasClass('passinput')) {
        password = $('#password-reset').val();
      }
      if (passowrdConfirm !== password && password.length > 0) {
        passowrdConfirmElement.next().html("Passowrd doesn't match");
        passowrdConfirmElement.next().show();
      }
      else {
        passowrdConfirmElement.next().hide();
      }

    }
  }
  changePassword() {
    const data = this.changePasswordForm.value;
    let isError = false;
    this.changePasswordForm.controls['token'].setValue(this.cookie.get('token'))

    for (let i = 0; i < $('.requird-input').length; i++) {
      if (!$($('.requird-input')[i]).val()) {
        $($('.requird-input')[i]).next().html('This feild is required');
        $($('.requird-input')[i]).next().show();
        isError = true;
      }
    }
    if (data['oldPassword'] === data['newPassword']) {
      $('#newPassowordFeedback').next().html('The new password can not be the same old password');
      $('#newPassowordFeedback').next().show();
      isError = true;

    }
    if (data['newPasswordConfirm'] !== data['newPassword']) {
      $('#passowordConfirmFeedback').next().html('Passwords dose not match');
      $('#passowordConfirmFeedback').next().show();
      isError = true;

    }

    // if (this.changePasswordForm.invalid || isError) {
    //   console.log('return')
    //   return;
    // }
    this.http.post('http://localhost:8000/user/change-password', data).subscribe(res => {
      if (!res['isError']) {
        UIkit.notification({
          message: 'Your passwrd has changed successfully.',
          pos: 'bottom-center',
          timeout: 4000
        });
      }
      else {
        $("#change-password-feedback").html(res['message']);
        $("#change-password-feedback").show();

      }
    })

  }
  changePasswordAPI(data) {
    return this.http.post<any>('http://localhost:8000/user/change-password', data)
  }
}