import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CreateVoteComponent } from './create-vote/create-vote.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { CookieService } from 'ngx-cookie-service';
import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { MyPollsComponent } from './my-polls/my-polls.component';
import { CastVoteComponent } from './cast-vote/cast-vote.component';
import { VoteResultComponent } from './vote-result/vote-result.component';
import { AuthGuard } from './auth-guard';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { Globals } from './global.config';
import { MyVotesComponent } from './my-votes/my-votes.component';
import { CastPrivateVoteComponent } from './private-vote/cast-private-vote/cast-private-vote.component';
import { PrivateVoteResultComponent } from './private-vote/private-vote-result/private-vote-result.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateVoteComponent,
    HeaderComponent,
    AboutComponent,
    MyPollsComponent,
    CastVoteComponent,
    VoteResultComponent,
    PasswordResetComponent,
    EditProfileComponent,
    MyVotesComponent,
    CastPrivateVoteComponent,
    PrivateVoteResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [Globals, PasswordResetComponent, HeaderComponent, CookieService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
