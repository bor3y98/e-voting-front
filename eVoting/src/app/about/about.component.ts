import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(public router: Router) {
    if (this.router.url === '/about') {
      $(".header-li").removeClass('active');
      $('.header-li.about').addClass('active')
    }
  }

  ngOnInit(): void {
  }

}
