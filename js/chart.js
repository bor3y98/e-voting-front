
        var ctx = document.getElementById('chartCanvas').getContext('2d');
        var s="";
        var data={
         labels:[""],
         datasets:[
             {
                 label:"person1",
                 data:[0.8],
                 backgroundColor:[
                    "#7f5fcb",
                 ],
                 
                 borderColor:[
                    "#7f5fcb",
                 ],
                 
             },
             
             {
                label:"person2",
                data:[0.3,0.6],
                backgroundColor:[
                    "#313153",
                    
                ],
                
                borderColor:[
                    "#313153",

                ],
               
            }
         ]
        };

        var options={
            title:{
                display:true,
                position:"top",
                fontSize:18,
                fontColor:"#111"
            },
    
            legend:{
                display:true,
                position:"bottom",
                labels: {
                fontColor:"red",
                }
            },

            scales:{
                yAxes:[{
                    ticks:{
                        min:0,
                        max:1.0,
                        stepSize:0.2
                    }

                }],

                xAxes: [{
                    categoryPercentage: 0.5,
                     barPercentage: 0.5
                }],
            }


            
    
           };

        var chart=new Chart(ctx,{
            type:"bar",
            data:data,
            options:options,
            
        
        });